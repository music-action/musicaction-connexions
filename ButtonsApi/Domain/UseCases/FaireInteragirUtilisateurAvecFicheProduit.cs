using MusicActionApi.Domain.FunctionalCore.Entities;

namespace MusicActionApi.Domain.UseCases;

// USE CASE (cas d'utilisation):
// je veux pouvoir connaitre les bouytons qui doivent s'afficher selon un contexte
// le contexte n'est pas encore bien défini encore, mais ce sera un mix de
// - qui je suis
// - sur quel produit je me trouve
// - ce que j'ai dejà fait comme actions passées (est ce que j'ai déjà activé tel ou tel bouton)

// TECHNIQUEMENT:  un UseCase est une classe qui va mettre en oeuvre différents "partenaires" pour arriver à faire ce qui est demandé
// entre autres, elle va demander des choses à des Adapteurs, notamment les adapteurs de la persistence
public class FaireInteragirUtilisateurAvecFicheProduit
{
    public IList<Bouton> ObtenirLesBoutonsVisiblesParToutLeMonde()
    {
        throw new NotImplementedException();
    }

    public IList<Bouton> ObtenirLesBoutonsVisiblesParUtilisateurConnecte(string email)
    {
        throw new NotImplementedException();
    }
}