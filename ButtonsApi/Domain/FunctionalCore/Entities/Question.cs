using MusicActionApi.Domain.FunctionalCore.ValueObjects;

namespace MusicActionApi.Domain.FunctionalCore.Entities;

public record Question(Email auteur, string contenu)
{
}