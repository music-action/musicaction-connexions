# Entités (DDD)

les entités sont des objets métiers, qui assurent la construction, la validation et les invariants

ils ne peuvent pas se loger tels quels dans la couche de persistance, il faut les copier dans des DTO
non plus, on ne peut se servir de ces objets pour les sérialiser dans l'API, là aussi il faut passer par la copie vers
des DTO

AutoMapper est là pour faire le (sale) boulot

https://dev.to/moe23/net-6-automapper-data-transfer-objects-dtos-49e

A ce stade du projet, il n'y a pas encore d'entité qui émerge (yagni)