namespace MusicActionApi.Domain.FunctionalCore.ValueObjects;

public record Email(string valeur)
{
    public bool Valider()
    {
        return valeur.Contains("@");
    }
}