using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MusicActionApi.Adapters.DrivenAdapters.SQLpersistence.DTO;
using MusicActionApi.Adapters.DriverAdapters.RestAPI.Models;
using MusicActionApi.HigherAbstractions.General;

namespace MusicActionApi.Adapters.DriverAdapters.RestAPI.Controllers;

[ApiController]
[Route(BaseRoute)]
public class QuestionsController : ControllerBase
{
    public const string BaseRoute = "api/questions";
    private readonly IMapper _mapper;

    public QuestionsController(ICanAccessPersistence<QuestionDTO> dataAccessAdapter)
    {
        this.dataAccessAdapter = dataAccessAdapter;
        var config = new MapperConfiguration(cfg =>
        {
            cfg
                .CreateMap<QuestionDTO, QuestionViewDto>()
                .ConstructUsing(source => new QuestionViewDto(source.Id, source.productReference, source.questionText,
                    source.userEmail, source.timestamp, source.isValid));
        });
        _mapper = config.CreateMapper();
    }

    public ICanAccessPersistence<QuestionDTO> dataAccessAdapter { get; }

    // Methode Poser pour poser une question lors de l'appui sur le bouton soumettre
    // Ajout dans la table QUESTION_PRODUIT dans la base de données
    [HttpPost]
    public async Task<IActionResult> Poser([FromBody] QuestionPostDto question, CancellationToken token)
    {
        var questionDto = new QuestionDTO(Ulid.NewUlid().ToString(), question.productReference, question.questionText, question.userEmail, DateTime.Now, false);
        var resultatTryAsync = await dataAccessAdapter.Insert(questionDto, token);
        var result = resultatTryAsync.Match<IActionResult>(
            question => Ok(question),
            err => BadRequest(err.Message));
        return result;
    }
    
    [HttpPost("populate")]
    public async Task<IActionResult> Populate(CancellationToken token)
    {
        await dataAccessAdapter.Insert(new QuestionDTO(Ulid.NewUlid().ToString(), "User1", "Comment accorder une guit", "emailm10@gmail.com", DateTime.Now, false), token);
        await dataAccessAdapter.Insert(new QuestionDTO(Ulid.NewUlid().ToString(), "User2", "Quelle interface audio est la meilleure pour les débutants?", "email11@gmail.com", DateTime.Now, false), token);
        await dataAccessAdapter.Insert(new QuestionDTO(Ulid.NewUlid().ToString(), "User3", "Quels sont les meilleurs plugins VST pour la production musicale?", "email12@gmail.com", DateTime.Now, false), token);
        await dataAccessAdapter.Insert(new QuestionDTO(Ulid.NewUlid().ToString(), "User4", "Comment améliorer la qualité de l'enregistrement vocal?", "email13@gmail.com", DateTime.Now, false), token);
        await dataAccessAdapter.Insert(new QuestionDTO(Ulid.NewUlid().ToString(), "User5", "Quelle est la différence entre un micro à condensateur et un micro dynamique?", "email14@gmail.com", DateTime.Now, false), token);
        await dataAccessAdapter.Insert(new QuestionDTO(Ulid.NewUlid().ToString(), "User6", "Comment jouer les accords de base sur un piano?", "email15@gmail.com", DateTime.Now, false), token);
        await dataAccessAdapter.Insert(new QuestionDTO(Ulid.NewUlid().ToString(), "User7", "Quels exercices de guitare sont bons pour les débutants?", "email16@gmail.com", DateTime.Now, false), token);
        await dataAccessAdapter.Insert(new QuestionDTO(Ulid.NewUlid().ToString(), "User8", "Comment nettoyer correctement une flûte traversière?", "email17@gmail.com", DateTime.Now, false), token);
        await dataAccessAdapter.Insert(new QuestionDTO(Ulid.NewUlid().ToString(), "User9", "Quelles sont les meilleures applications pour apprendre la musique?", "email18@gmail.com", DateTime.Now, false), token);
        await dataAccessAdapter.Insert(new QuestionDTO(Ulid.NewUlid().ToString(), "User10", "Quels sont les critères pour choisir un bon amplificateur de guitare?", "email19@gmail.com", DateTime.Now, false), token);
        await dataAccessAdapter.Insert(new QuestionDTO(Ulid.NewUlid().ToString(), "User11", "Comment faire des enregistrements multicouches chez soi?", "email20@gmail.com", DateTime.Now, false), token);
        await dataAccessAdapter.Insert(new QuestionDTO(Ulid.NewUlid().ToString(), "User12", "Quel est le meilleur logiciel de notation musicale?", "email21@gmail.com", DateTime.Now, false), token);
        await dataAccessAdapter.Insert(new QuestionDTO(Ulid.NewUlid().ToString(), "User13", "Comment équilibrer les basses et les aigus dans un mix?", "email22@gmail.com", DateTime.Now, false), token);
        await dataAccessAdapter.Insert(new QuestionDTO(Ulid.NewUlid().ToString(), "User14", "Quelle est la meilleure manière de mémoriser des partitions?", "email23@gmail.com", DateTime.Now, false), token);
        await dataAccessAdapter.Insert(new QuestionDTO(Ulid.NewUlid().ToString(), "User15", "Comment synchroniser des lumières avec de la musique en live?", "email24@gmail.com", DateTime.Now, true), token);
        await dataAccessAdapter.Insert(new QuestionDTO(Ulid.NewUlid().ToString(), "User16", "Quels types de microphones sont recommandés pour l'enregistrement de podcasts?", "email25@gmail.com", DateTime.Now, true), token);
        var resultat = await dataAccessAdapter.Insert(new QuestionDTO(Ulid.NewUlid().ToString(), "User17", "Quel est le meilleur synthétiseur pour débutants?", "email26@gmail.com", DateTime.Now, true), token);
        var result = resultat.Match<IActionResult>(
            question => Ok(question),
            err => BadRequest(err.Message));
        return result;
    }

    [HttpGet]
    public async Task<IActionResult> GetQuestions(CancellationToken token)
    {
        var result = await dataAccessAdapter.GetAll(token);
        
        var questions = result.Match(
            Left: error => new List<QuestionDTO>(), 
            Right: questionsList => questionsList 
        );
        return Ok(questions);
    }
    
    [HttpGet("valid/pagination/{page}/{pageSize}")]
    public async Task<IActionResult> GetQuestionsValidPaging(int page, int pageSize, CancellationToken token)
    {
        var result = await dataAccessAdapter.GetPaging(x => x.isValid == true, pageSize, page, token);
        
        var questions = result.Match(
            Left: error => new List<QuestionDTO>(), 
            Right: questionsList => questionsList 
        );
        
        return Ok(questions);
    }
    
    [HttpGet("valid")]
    public async Task<IActionResult> GetQuestionsValid(CancellationToken token)
    {
        var result = await dataAccessAdapter.Get(x => x.isValid == true, token);
        
        var questions = result.Match(
            Left: error => new List<QuestionDTO>(), 
            Right: questionsList => questionsList 
        );
        
        return Ok(questions);
    }
    
    [HttpGet("valid/count")]
    public async Task<IActionResult> GetQuestionsValidCount(CancellationToken token)
    {
        var result = await dataAccessAdapter.Get(x => x.isValid == true, token);
        
        var questions = result.Match(
            Left: error => 0, 
            Right: questionsList => questionsList.Count()
        );
        
        return Ok(questions);
    }
    
    [HttpGet("notValid/pagination/{page}/{pageSize}")]
    public async Task<IActionResult> GetQuestionsNotValidPaging(int page, int pageSize, CancellationToken token)
    {
        var result = await dataAccessAdapter.GetPaging(x => x.isValid != true, pageSize, page, token);
        
        var questions = result.Match(
            Left: error => new List<QuestionDTO>(), 
            Right: questionsList => questionsList 
        );
        
        return Ok(questions);
    }
    
    [HttpGet("notValid")]
    public async Task<IActionResult> GetQuestionsNotValid(CancellationToken token)
    {
        var result = await dataAccessAdapter.Get(x => x.isValid != true, token);
        
        var questions = result.Match(
            Left: error => new List<QuestionDTO>(), 
            Right: questionsList => questionsList 
        );
        
        return Ok(questions);
    }

    [HttpGet("notValid/count")]
    public async Task<IActionResult> GetQuestionsNotValidCount(CancellationToken token)
    {
        var result = await dataAccessAdapter.Get(x => x.isValid != true, token);

        var questions = result.Match(
            Left: error => 0,
            Right: questionsList => questionsList.Count()
        );

        return Ok(questions);
    }

    [HttpDelete("delete/{id}")]
    public async Task<IActionResult> DeleteQuestion(string id, CancellationToken token)
    {
        var question = await dataAccessAdapter.GetById(id, token);
        var questionDto = _mapper.Map<QuestionDTO>(question);
        var resultatTryAsync = await dataAccessAdapter.Delete(id, token);
        var result = resultatTryAsync.Match<IActionResult>(
            question => Ok(questionDto),
            err => BadRequest(err.Message));
        return result;
    }
    
    [HttpPut("validate/{id}")]
    public async Task<IActionResult> ValidateQuestion(string id, CancellationToken token)
    {
        var question = await dataAccessAdapter.GetById(id, token);
        var questionDto = _mapper.Map<QuestionDTO>(question);
        var newQuestionDto = new QuestionDTO(questionDto.Id, questionDto.productReference, questionDto.questionText,
            questionDto.userEmail, questionDto.timestamp, true);
        var resultatTryAsync = await dataAccessAdapter.Put(newQuestionDto, token);
        var result = resultatTryAsync.Match<IActionResult>(
            question => Ok(question),
            err => BadRequest(err.Message));
        return result;
    }

    [HttpGet("hardcoded")]
    public IActionResult GetHardcodedValues()
    {
        // Création d'une liste d'objets BoutonDTO avec des valeurs en dur
        var hardcodedButtons = new List<QuestionDTO>
        {
            new("1", "User1", "Comment accorder une guit", "emailm10@gmail.com", DateTime.Now, true),
            new("2", "User2", "Quelle interface audio est la meilleure pour les débutants?", "email11@gmail.com",
                DateTime.Now, true),
            new("3", "User3", "Quels sont les meilleurs plugins VST pour la production musicale?", "email12@gmail.com",
                DateTime.Now, true),
            new("4", "User4", "Comment améliorer la qualité de l'enregistrement vocal?", "email13@gmail.com",
                DateTime.Now, true),
            new("5", "User5", "Quelle est la différence entre un micro à condensateur et un micro dynamique?",
                "email14@gmail.com", DateTime.Now, true),
            new("6", "User6", "Comment jouer les accords de base sur un piano?", "email15@gmail.com", DateTime.Now, true),
            new("7", "User7", "Quels exercices de guitare sont bons pour les débutants?", "email16@gmail.com",
                DateTime.Now, true),
            new("8", "User8", "Comment nettoyer correctement une flûte traversière?", "email17@gmail.com",
                DateTime.Now, true),
            new("9", "User9", "Quelles sont les meilleures applications pour apprendre la musique?",
                "email18@gmail.com", DateTime.Now, true),
            new("10", "User10", "Quels sont les critères pour choisir un bon amplificateur de guitare?",
                "email19@gmail.com", DateTime.Now, true),
            new("11", "User11", "Comment faire des enregistrements multicouches chez soi?", "email20@gmail.com",
                DateTime.Now, true),
            new("12", "User12", "Quel est le meilleur logiciel de notation musicale?", "email21@gmail.com",
                DateTime.Now, true),
            new("13", "User13", "Comment équilibrer les basses et les aigus dans un mix?", "email22@gmail.com",
                DateTime.Now, true),
            new("14", "User14", "Quelle est la meilleure manière de mémoriser des partitions?", "email23@gmail.com",
                DateTime.Now, true),
            new("15", "User15", "Comment synchroniser des lumières avec de la musique en live?", "email24@gmail.com",
                DateTime.Now, true),
            new("16", "User16", "Quels types de microphones sont recommandés pour l'enregistrement de podcasts?",
                "email25@gmail.com", DateTime.Now, true),
            new("17", "User17", "Quel est le meilleur synthétiseur pour débutants?", "email26@gmail.com", DateTime.Now, true)
        };
        // Renvoi des objets de vue en tant que réponse
        return Ok(hardcodedButtons);
    }
}