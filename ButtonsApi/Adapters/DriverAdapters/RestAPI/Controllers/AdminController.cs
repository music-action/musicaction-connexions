using Microsoft.AspNetCore.Mvc;

namespace MusicActionApi.Adapters.DriverAdapters.RestAPI.Controllers;

[ApiController]
[Route(BaseRoute)]
public class AdminController
{
    private const string BaseRoute = "api/user";

    [HttpGet("pageAccueil")]
    public IActionResult GetPageAccueil(CancellationToken token)
    {
        throw new NotImplementedException();
        //Vérifier le JWT et si le role est bien admin
        //si oui, retourner la page d'accueil
        //sinon, rediriger vers la page de login
    }
    
    [HttpPost("credentials/{email}")]
    public async Task<IActionResult> credentials(String email, CancellationToken token)
    {
        throw new NotImplementedException();
        //Vérifier si l'email est bien dans la base de données
        //génére un code temporaire et l'envoi par mail
    }
}