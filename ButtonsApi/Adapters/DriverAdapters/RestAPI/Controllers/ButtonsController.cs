using System.Text.Json;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MusicActionApi.Adapters.DrivenAdapters.SQLpersistence.DTO;
using MusicActionApi.Adapters.DriverAdapters.RestAPI.Models;
using MusicActionApi.HigherAbstractions.General;

namespace MusicActionApi.Adapters.DriverAdapters.RestAPI.Controllers;

[ApiController]
[Route(BaseRoute)]
public class ButtonsController : ControllerBase
{
    public const string BaseRoute = "api/buttons";
    private readonly ILogger<ButtonsController> _logger;
    private readonly IMapper _mapper;

    public ButtonsController(ICanAccessPersistence<BoutonDTO> dataAccessAdapter, ILogger<ButtonsController> logger)
    {
        _dataAccessAdapter = dataAccessAdapter;
        var config = new MapperConfiguration(cfg =>
        {
            cfg
                .CreateMap<BoutonDTO, BoutonViewDto>()
              //  .ForMember( destination => destination.Order, )
                .ConstructUsing( source => new BoutonViewDto( source.Id, source.buttonText , source.actionURL)) 
                .AfterMap((src, dest) => dest.ActionRoute = $"{BaseRoute}/{src.Id}");
        });
        _mapper = config.CreateMapper();
        _logger = logger;
    }

    public ICanAccessPersistence<BoutonDTO> _dataAccessAdapter { get; }


    [HttpGet("hello")]
    public IActionResult Test()
    {
        return Ok();
    }

    [HttpGet]
    public async Task<IActionResult> GetAll(CancellationToken token)
    {
        //  la seule responsabilité du controleur c'est transformer ce qui vient des requêtes HTTP
        // que ce soit stockés dans la querystring, ou dans le Body ou dans les Headers de la requêtes
        // https://en.wikipedia.org/wiki/Query_string
        // https://developer.mozilla.org/en-US/docs/Web/HTTP/Messages
        // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers

        // deserialisation =>  DTO  (data transfer object), utiliser un Record
        // A LIRE https://professionalbeginner.com/the-dto-dilemma

        // c'est très temporaire, on ne doit pas appeler le dataAdapter ici
        // on doit passer par le UseCase normalement
        _logger.LogDebug("GetAll method started");
        var resultatTryAsync
            = _dataAccessAdapter.GetAll(token);

        _logger.LogDebug($"Received data from the adapter: {resultatTryAsync}");
        //je fais le map du Ethier et le mapping d'un DTO vers l'autre 
        var resultatDuGetAllMappéVersLesViewDTO
            = resultatTryAsync.Map(
                listeBoutonDto =>
                    listeBoutonDto.Select(b => _mapper.Map<BoutonDTO, BoutonViewDto>(b))
            );

        var result = resultatDuGetAllMappéVersLesViewDTO.Match<IActionResult>(
            allButtonsForView =>
            {
                _logger.LogDebug("Successfully mapped data for view.");
                return  Ok( allButtonsForView );
            },
            err =>
            {
                _logger.LogError($"Error occurred: {err.Message}");
                return BadRequest(err.Message);
            });
        return await result;
    }
}