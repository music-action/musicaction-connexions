using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MusicActionApi.Adapters.DrivenAdapters.SQLpersistence.DTO;
using MusicActionApi.Adapters.DriverAdapters.RestAPI.Models;
using MusicActionApi.HigherAbstractions.General;

namespace MusicActionApi.Adapters.DriverAdapters.RestAPI.Controllers;

[ApiController]
[Route(BaseRoute)]
public class UserController(ICanAccessPersistence<UserDTO> dataAccessAdapter, IMapper mapper)
    : ControllerBase
{
    private const string BaseRoute = "api/user";

    private ICanAccessPersistence<UserDTO> dataAccessAdapter { get; } = dataAccessAdapter;
    
    [HttpGet("listAdmin")]
    public async Task<IActionResult> GetAdmin(CancellationToken token)
    {
        var admins = await dataAccessAdapter.Get(x => x.isAdmin == true, token);
        var userDtos = mapper.Map<IEnumerable<UserDTO>>(admins);
        return Ok(userDtos);
    }

    [HttpPost("{id}/add")]
    public async Task<IActionResult> AddAdmin(string id, CancellationToken token)
    {
        var user = await dataAccessAdapter.GetById(id, token);
        var userDto = mapper.Map<UserDTO>(user);
        var newUserDto = new UserDTO(userDto.Id, userDto.email, true);
        var resultatTryAsync = await dataAccessAdapter.Put(newUserDto, token);
        var result = resultatTryAsync.Match<IActionResult>(
            user => Ok(user),
            err => BadRequest(err.Message));
        return result;
    }

    [HttpPut("{id}/revoke")]
    public async Task<IActionResult> RevokeAdmin(string id, CancellationToken token)
    {
        var user = await dataAccessAdapter.GetById(id, token);
        var userDto = mapper.Map<UserDTO>(user);
        var newUserDto = new UserDTO(userDto.Id, userDto.email, false);
        var resultatTryAsync = await dataAccessAdapter.Put(newUserDto, token);
        var result = resultatTryAsync.Match<IActionResult>(
            user => Ok(user),
            err => BadRequest(err.Message));
        return result;
    }
}