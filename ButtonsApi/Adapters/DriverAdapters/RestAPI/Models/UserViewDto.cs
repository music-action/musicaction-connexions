namespace MusicActionApi.Adapters.DriverAdapters.RestAPI.Models;

public record UserViewDto
{
    public UserViewDto(string Id, string email, Boolean isAdmin)
    {
        this.Id = Id;
        this.email = email;
        this.isAdmin = isAdmin;
    }
    
    public string Id { get; init; }
    public string email { get; init; }
    public Boolean isAdmin { get; init; }
    
    public void Deconstruct(out string Id, out string email, out Boolean isAdmin)
    {
        Id = this.Id;
        email = this.email;
        isAdmin = this.isAdmin;
    }
}