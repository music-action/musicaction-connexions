namespace MusicActionApi.Adapters.DriverAdapters.RestAPI.Models;

public record QuestionViewDto
{
    public QuestionViewDto(string question_id, string ProductReference, string QuestionText, string UserEmail,
        DateTime Timestamp, Boolean IsValid)
    {
        this.question_id = question_id;
        this.ProductReference = ProductReference;
        this.QuestionText = QuestionText;
        this.UserEmail = UserEmail;
        this.Timestamp = Timestamp;
        this.IsValid = IsValid;
    }

    
    public string question_id { get; init; }
    public string ProductReference { get; init; }
    public string QuestionText { get; init; }
    public string UserEmail { get; init; }
    public DateTime Timestamp { get; init; }
    public bool IsValid { get; init; }

    public void Deconstruct(out string question_id, out string ProductReference, out string QuestionText,
        out string UserEmail, out DateTime Timestamp, out Boolean IsValid)
    {
        question_id = this.question_id;
        ProductReference = this.ProductReference;
        QuestionText = this.QuestionText;
        UserEmail = this.UserEmail;
        Timestamp = this.Timestamp;
        IsValid = this.IsValid;
    }
}