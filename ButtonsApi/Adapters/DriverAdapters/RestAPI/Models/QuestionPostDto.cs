namespace MusicActionApi.Adapters.DriverAdapters.RestAPI.Models;

public record QuestionPostDto(string productReference, string questionText, string userEmail)
{
}