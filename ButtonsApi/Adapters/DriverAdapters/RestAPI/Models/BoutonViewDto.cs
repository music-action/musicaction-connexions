namespace MusicActionApi.Adapters.DriverAdapters.RestAPI.Models;

/// <summary>
///     il s'agit d'avoir l'information nécessaire à l'affichage de boutons "actifs" dans une IHM Web
/// </summary>
public class BoutonViewDto(  string Order ,string Name , string ActionRoute)
{
    public string Order { get; init; } = Order;
    public string Name { get; init; } = Name;
    public string ActionRoute { get; set; } = ActionRoute;

   
}