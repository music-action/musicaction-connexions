using MongoDB.Driver;

namespace MusicActionApi.Adapters.DrivenAdapters.MongoDBPersistence;

public class MongoDBConnection
{
    private readonly IMongoClient _client;
    private readonly IMongoDatabase _database;

    public MongoDBConnection(string connectionString, string databaseName)
    {
        _client = new MongoClient(connectionString);
        _database = _client.GetDatabase(databaseName);
    }

    public IMongoCollection<T> GetCollection<T>(string collectionName)
    {
        return _database.GetCollection<T>(collectionName);
    }
}