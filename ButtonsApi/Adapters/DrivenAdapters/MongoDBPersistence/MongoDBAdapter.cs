using System.Linq.Expressions;
using System.Text.RegularExpressions;
using LanguageExt;
using LanguageExt.Common;
using MongoDB.Driver;
using MusicActionApi.HigherAbstractions.General;
using MusicActionApi.HigherAbstractions.Models;
using static LanguageExt.Prelude;

namespace MusicActionApi.Adapters.DrivenAdapters.MongoDBPersistence;

public class MongoDBAdapter<T>(MongoDBConnection mongoDbConnection) : ICanAccessPersistence<T>
    where T : DtoWithId
{
    private readonly IMongoCollection<T> _collection = mongoDbConnection.GetCollection<T>(typeof(T).Name);

    public EitherAsync<Error, int> PopulateTable(CancellationToken token)
    {
        //A CHANGER
        return EitherAsync<Error, int>.Right(3);
    }

    public EitherAsync<Error, int> CreateTable(CancellationToken token)
    {
        return EitherAsync<Error, int>.Right(1);
    }

    public EitherAsync<Error, IEnumerable<T>> GetAll(CancellationToken token)
    {
        return TryAsync
            (async () =>
                await
                    IAsyncCursorSourceExtensions.ToListAsync(_collection.AsQueryable(), token)
            )
            .ToEither()
            .Map(list => list.AsEnumerable());
    }

    //vérifier que l'élément n'existe pas déjà
    public EitherAsync<Error, T> Insert(T item, CancellationToken token)
    {
        return GetById(item.Id, token).BiBind(
            value => EitherAsync<Error, T>.Left(Error.New($"Un élément avec l'identifiant {item.Id} existe déjà")),
            error => error.Message == $"L'identifiant {item.Id} n'est pas un ULID valide"
                ? EitherAsync<Error, T>.Left(error)
                : TryAsync(async () =>
                        await _collection.ReplaceOneAsync(x => x.Id == item.Id, item,
                            new ReplaceOptions { IsUpsert = true }, token)
                    )
                    .ToEither()
                    .Map(_ => item)
        );
    }


    public EitherAsync<Error, T> Put(T item, CancellationToken token)
    {
        return GetById(item.Id, token).Bind(value =>
            TryAsync(async () =>
                    await _collection.ReplaceOneAsync(x => x.Id == item.Id, item,
                        new ReplaceOptions { IsUpsert = true }, token)
                )
                .ToEither()
                .Map(_ => item)
        );
    }

    public EitherAsync<Error, T> GetById(string primaryKey, CancellationToken token)
    {
        var ulidPattern = "^[0-9A-Za-z]{26}$";
        if (Regex.IsMatch(primaryKey, ulidPattern))
            return TryAsync
                (async () =>
                    await
                        _collection.Find(x => x.Id == primaryKey)
                            .FirstAsync(token)
                )
                .ToEither()
                .MapLeft(err => Error.New(
                    $"L'identifiant {primaryKey} n'existe pas dans la collection {typeof(T).Name}"));
        return EitherAsync<Error, T>
            .Left(Error.New($"L'identifiant {primaryKey} n'est pas un ULID valide"));
    }

    public EitherAsync<Error, IEnumerable<T>> Get(Expression<Func<T, bool>> predicated, CancellationToken token)
    {
        return TryAsync(async () =>
        {
            var cursor = await _collection.FindAsync(predicated, cancellationToken: token);
            var list = await cursor.ToListAsync(token);
            return list.Select(item => (T)item).AsEnumerable();
        }).ToEither();
    }
    
    public EitherAsync<Error, int> Delete(string primaryKey, CancellationToken token)
    {
        return GetById(primaryKey, token).Bind(value =>
            TryAsync(async () =>
            {
                var result = await _collection.DeleteOneAsync(x => x.Id == primaryKey, token);
                return (int)result.DeletedCount;
            }).ToEither()
        );
    }
    
    public EitherAsync<Error, IEnumerable<T>> GetPaging(Expression<Func<T, bool>> predicated, int numberPerPage, int pageNumber, CancellationToken token)
    {
        var toSkip = numberPerPage * (pageNumber - 1);
        var options = new FindOptions<T> { Skip = toSkip, Limit = numberPerPage };
        return TryAsync(async () =>
            {
            var cursor = await _collection.FindAsync(predicated, options, token);
            var documents = await cursor.ToListAsync(token);
            return documents.Select(item => (T)item).AsEnumerable();
        }).ToEither();
    }
}