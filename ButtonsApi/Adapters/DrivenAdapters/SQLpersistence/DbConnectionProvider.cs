using System.Data.Common;
using Npgsql;

namespace MusicActionApi.Adapters.DrivenAdapters.SQLpersistence;

public sealed class DbConnectionProvider
{
    private readonly string _connectionString;

    public DbConnectionProvider(string connectionString)
    {
        _connectionString = connectionString;
    }

    public DbConnection GetConnection()
    {
        return new NpgsqlConnection(_connectionString);
    }

    public NpgsqlConnection GetNpgsqlConnection()
    {
        return new NpgsqlConnection(_connectionString);
    }


    // méthode Migrate() à utiliser pour le démarrage de l'application ASP.Net CORE ici ?
}