using MongoDB.Bson.Serialization.Attributes;
using MusicActionApi.HigherAbstractions.Models;

namespace MusicActionApi.Adapters.DrivenAdapters.SQLpersistence.DTO;

public record AdminDTO(
    [property: BsonId] string Id,
    string adminEmail,
    string temporaryCode,
    DateTime CodeExpirationDateTime) : DtoWithId
{
}