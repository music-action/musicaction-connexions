using MongoDB.Bson.Serialization.Attributes;
using MusicActionApi.HigherAbstractions.Models;

namespace MusicActionApi.Adapters.DrivenAdapters.SQLpersistence.DTO;

public record QuestionDTO(
    [property: BsonId] string Id,
    string productReference,
    string questionText,
    string userEmail,
    DateTime timestamp,
    Boolean isValid) : DtoWithId
{
}