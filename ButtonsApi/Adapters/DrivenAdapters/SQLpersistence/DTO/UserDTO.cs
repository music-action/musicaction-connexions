using MongoDB.Bson.Serialization.Attributes;
using MusicActionApi.HigherAbstractions.Models;

namespace MusicActionApi.Adapters.DrivenAdapters.SQLpersistence.DTO;
public record UserDTO([property: BsonId] string Id, string email, Boolean isAdmin) : DtoWithId
{
}