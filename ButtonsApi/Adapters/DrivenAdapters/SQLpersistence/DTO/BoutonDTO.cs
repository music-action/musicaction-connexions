using MongoDB.Bson.Serialization.Attributes;
using MusicActionApi.HigherAbstractions.Models;

namespace MusicActionApi.Adapters.DrivenAdapters.SQLpersistence.DTO;

public record BoutonDTO([property: BsonId] string Id, string buttonText, string verb, string actionURL) : DtoWithId
{
}