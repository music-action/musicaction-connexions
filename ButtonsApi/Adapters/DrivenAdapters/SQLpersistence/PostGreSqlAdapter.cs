using System.Data;
using System.Linq.Expressions;
using Dapper;
using LanguageExt;
using LanguageExt.Common;
using MusicActionApi.Adapters.DrivenAdapters.SQLpersistence.DTO;
using MusicActionApi.HigherAbstractions.General;
using MusicActionApi.HigherAbstractions.Models;
using SqlKata.Compilers;
using SqlKata.Execution;
using static LanguageExt.Prelude;

namespace MusicActionApi.Adapters.DrivenAdapters.SQLpersistence;

public class PostGreSqlAdapter<T>(DbConnectionProvider dbConnectionProvider)
    : ICanAccessPersistence<T> where T : DtoWithId
{
    [Obsolete("on préfère utiliser DB-UP")]
    public EitherAsync<Error, int> PopulateTable(CancellationToken token)
    {
        var resultatLireTousLesItems = GetAll(token);
        EitherAsync<Error, int> res;
        switch (typeof(T).Name)
        {
            case "BoutonDto":
                res = resultatLireTousLesItems.Bind(lst => InsertSetOfButtons(lst, token));
                break;
            case "QuestionDto":
                res = resultatLireTousLesItems.Bind(lst => InsertSetOfQuestions(lst, token));
                break;
            // Ajoutez d'autres cas pour d'autres types si nécessaire
        }

        return res;
    }

    [Obsolete("on préfère utiliser DB-UP")]
    public EitherAsync<Error, int> CreateTable(CancellationToken token)
    {
        var res = TryAsync(
            async () =>
            {
                var typeName = typeof(T).Name;
                await using var connection = dbConnectionProvider.GetConnection();
                var tableName = GetTableName<T>();
                var primaryKey = GetPrimaryKey(typeName);
                // Générez la clause de création de colonne pour les propriétés du type T
                var properties = typeof(T).GetProperties()
                    .Select(property =>
                    {
                        var columnName = property.Name;
                        var columnType = GetColumnType(property.PropertyType);
                        if (columnName == primaryKey)
                            return null;
                        return
                            $"{columnName} {columnType} NOT NULL"; // Format de la colonne: nom_colonne type_colonne NOT NULL (à changer si null possible pour certaines colonnes)
                    });
                var columns = string.Join(", ", properties);

                var sql = $"CREATE TABLE IF NOT EXISTS {tableName} " +
                          $"({primaryKey} SERIAL PRIMARY KEY {columns})";
                if (connection.State != ConnectionState.Open)
                    await connection.OpenAsync();

                return await connection.ExecuteAsync(sql).WaitAsync(token);
            });
        return res.ToEither();
    }

    public EitherAsync<Error, IEnumerable<T>> GetAll(CancellationToken token)
    {
        var res = TryAsync(async () => await
            dbConnectionProvider.GetNpgsqlConnection()
                .QueryAsync<T>($"SELECT * FROM {GetTableName<T>()}")
                .WaitAsync(token));
        return res.ToEither();
    }

    public EitherAsync<Error, T> Insert(T item, CancellationToken token)
    {
        var res = TryInsert(item, token).ToEither();
        var resSansNulll = res.Match(dto =>
            {
                if (dto == null)
                    return Either<Error, T>.Left(Error.New(
                        $"Impossible d'insérer l'élément dans la table {GetTableName<T>()} de l'élément {typeof(T).Name}"));
                return Either<Error, T>.Right(dto);
            },
            ex => Either<Error, T>.Left(ex.Message));
        return resSansNulll.ToAsync();
    }

    public EitherAsync<Error, T> GetById(string primaryKey, CancellationToken token)
    {
        var res = TryGetById(dbConnectionProvider, primaryKey, token).ToEither();
        var resSansNulll = res.Match(dto =>
            {
                if (dto == null)
                    return Either<Error, T>.Left(
                        Error.New($"La clé primaire {primaryKey} n'existe pas dans la table {GetTableName<T>()}"));
                return Either<Error, T>.Right(dto);
            },
            ex => Either<Error, T>.Left(ex.Message));
        // you can transform a task<either<l, r>> to an eitherasync<l, r> value with the toasync method:
        return resSansNulll.ToAsync();
    }

    public EitherAsync<Error, IEnumerable<T>> Get(Expression<Func<T, bool>> predicated, CancellationToken token)
    {
        throw new NotImplementedException();
    }

    public EitherAsync<Error, int> Delete(string primaryKey, CancellationToken token)
    {
        throw new NotImplementedException();
    }

    public EitherAsync<Error, IEnumerable<T>> GetPaging(Expression<Func<T, bool>> predicated, int numberPerPage, int pageNumber, CancellationToken token)
    {
        throw new NotImplementedException();
    }

    public EitherAsync<Error, T> Put(T item, CancellationToken token)
    {
        var itemExistant = GetById(GetPrimaryKeyValue(item), token);
        var res = itemExistant.BiBind(
            _ => Update(item, token),
            _ => Insert(item, token));
        return res;
    }


    private EitherAsync<Error, int> InsertSetOfButtons(IEnumerable<T> listeBoutons, CancellationToken token)
    {
        if (!listeBoutons.Any())
        {
            var instance = new PostGreSqlAdapter<BoutonDTO>(dbConnectionProvider);
            var insert1 = instance.Insert(new BoutonDTO("100", "Occaz", "POST", "/buttons/100/click"), token);
            var insert2 = insert1.Bind(_ =>
                instance.Insert(new BoutonDTO("101", "Aide support", "POST", "/buttons/101/click"), token));
            var insert3 = insert2.Bind(_ =>
                instance.Insert(new BoutonDTO("999", "Occaz Vente", "POST", "/buttons/999/click"), token));
            return EitherAsync<Error, int>.Right(3);
        }

        return EitherAsync<Error, int>.Right(0);
    }

    private EitherAsync<Error, int> InsertSetOfQuestions(IEnumerable<T> listeQuestion, CancellationToken token)
    {
        if (!listeQuestion.Any())
        {
            var instance = new PostGreSqlAdapter<QuestionDTO>(dbConnectionProvider);
            var insert1 =
                instance.Insert(
                    new QuestionDTO("12", "ABC123", "Comment jouer de cette guitare ?", "michel@email.com",
                        DateTime.Now, true), token);
            var insert2 = insert1.Bind(_ =>
                instance.Insert(
                    new QuestionDTO("13", "DEF456", "Ce modèle est il le meilleur rapport qualité prix ?",
                        "michel@email.com", DateTime.Now, true), token));
            var insert3 = insert2.Bind(_ =>
                instance.Insert(
                    new QuestionDTO("14", "GHI789", "Comment connecter cette ampli à ma guitare ?", "michel@email.com",
                        DateTime.Now, true), token));
            return EitherAsync<Error, int>.Right(3);
        }

        return EitherAsync<Error, int>.Right(0);
    }

    private TryAsync<T> TryInsert(T item, CancellationToken token)
    {
        var res = TryAsync(
            async () =>
            {
                await using var connection = dbConnectionProvider.GetNpgsqlConnection();
                if (connection.State != ConnectionState.Open) await connection.OpenAsync(token);

                // Create a new query factory
                var queryFactory = new QueryFactory(connection, new PostgresCompiler());

                // Construct the query using SQLKata
                var tableName = GetTableName<T>();
                var query = queryFactory.Query(tableName);
                // Obtenez les propriétés de l'objet item
                var properties = item.GetType().GetProperties();
                // gné????  getType() et GetProperties() font effondrer les performances
                // au moins, il faudrait créer un cache (un dictionnaire en mémoire)


                var columns = new Dictionary<string, object>();
                foreach (var property in properties) columns[property.Name] = property.GetValue(item);
                query.AsInsert(columns);
                // Compilation et exécution de la requête
                var compiler = new PostgresCompiler();
                var compiledQuery = compiler.Compile(query);
                await connection.ExecuteAsync(compiledQuery.Sql, compiledQuery.NamedBindings).WaitAsync(token);
                return item;
            });
        return res;
    }

    private static TryAsync<T?> TryGetById(DbConnectionProvider dbConnectionProvider, string primaryKey,
        CancellationToken token)
    {
        var typeName = typeof(T).Name;
        // non, non, non... interdit de concaténer des chaines de caractères pour faire une requête SQL
        // c'est la porte ouverte aux attaques par SQL injection
        // https://cheatsheetseries.owasp.org/cheatsheets/SQL_Injection_Prevention_Cheat_Sheet.html
        var sql = $"SELECT * FROM {GetTableName<T>()} WHERE {GetPrimaryKey(typeName)} = @primaryKeyValue";
        // 2 solutions
        //  Use Parameterized Statements
        // https://medium.com/@V-Blaze/sql-injection-protecting-your-postgresql-database-ce8c0cc43685

        // mais plus simple encore: utilise Dapper.SqlBuilder

        return TryAsync(
            async () =>
            {
                // ici, on ne mets que le code qui est susceptible de provoquer des exceptions
                await using var connection = dbConnectionProvider.GetNpgsqlConnection();
                if (connection.State != ConnectionState.Open)
                    await connection.OpenAsync(token);
                var res = await connection.QueryFirstOrDefaultAsync<T>(sql, new { primaryKeyValue = primaryKey })
                    .WaitAsync(token);
                return res;
            });
    }

    private EitherAsync<Error, T> Update(T item, CancellationToken token)
    {
        var res = TryUpdate(dbConnectionProvider, item, token).ToEither();
        var resSansNull = res.Match(
            dto =>
            {
                if (dto == null)
                    return Either<Error, T>.Left(Error.New(
                        $"Impossible de mettre à jour l'élément pour la table de l'élément {typeof(T).Name}"));
                return Either<Error, T>.Right(dto);
            },
            ex => Either<Error, T>.Left(ex.Message));
        return resSansNull.ToAsync();
    }

    private static TryAsync<T> TryUpdate(DbConnectionProvider dbConnectionProvider, T item,
        CancellationToken token)
    {
        var res = TryAsync(
            async () =>
            {
                await using var connection = dbConnectionProvider.GetNpgsqlConnection();
                var sql = BuildUpdateQuery<T>();
                if (connection.State != ConnectionState.Open)
                    await connection.OpenAsync(token);

                await connection.ExecuteAsync(sql, item).ConfigureAwait(false);
                return item;
            });
        return res;
    }

    private static string BuildUpdateQuery<T>()
    {
        //mmmmmmmmh.... il me semble que Dapper permet  de faire ca à ta place
        // https://github.com/DapperLib/Dapper/tree/main/Dapper.SqlBuilder
        // ou bien 
        // vous aviez commencé avec SqlKata... autant l'utiliser partout

        var tableName = GetTableName<T>();
        var properties = typeof(T).GetProperties();
        var setClause = string.Join(", ", properties.Select(p => $"{p.Name} = @{p.Name}"));

        var typeName = typeof(T).Name;
        var whereClause = $"{GetPrimaryKey(typeName)} = @{GetPrimaryKey(typeName)}";
        return $"UPDATE {tableName} SET {setClause} WHERE {whereClause}";

        //  aaah , ca me semble trop compliqué;
        // regarde ce code
        // https://github.com/SyncfusionExamples/ASP.NET-Core-Web-API-with-Dapper-and-PostgreSQL/blob/master/DotNetCRUD/Services/EmployeeService.cs
        // on en reparle
        // peut etre que le plus simple c'est de passer à MongoDB
    }

    private static string GetTableName<T>()
    {
        // au moins remplacer typeof(T).Name par typeName
        var typeName = typeof(T).Name; //ce typeName pourrait etre un parametre de la fonction

        return typeName switch
        {
            "BoutonDto" => "buttons",
            "QuestionDto" => "questions_produit",
            _ => throw new InvalidOperationException(
                $"Impossible de trouver le nom de la table pour le type {typeName}.")
        };
    }

    [Obsolete("DBUP va remplacer tout ca")]
    private static string GetColumnType(Type type)
    {
        return type.Name switch
        {
            "Int32" => "INTEGER",
            "String" => "VARCHAR",
            "DateTime" => "TIMESTAMP",
            // Ajoutez d'autres cas pour d'autres types si nécessaire
            _ => throw new InvalidOperationException(
                $"Impossible de trouver le type de colonne SQL pour le type {type.Name}.")
        };
    }

    private static string GetPrimaryKey(string typeName)
    {
        return typeName switch // new C#12 pattern matching, for better performance
        {
            "BoutonDto" => "shortid",
            "QuestionDto" => "question_id",
            _ => throw new InvalidOperationException(
                $"Impossible de trouver la clé primaire de la table pour le type {typeName}.")
        };
    }

    private string GetPrimaryKeyValue(T item)
    {
        var typeName = typeof(T).Name;
        var primaryKey = item.GetType().GetProperty(GetPrimaryKey(typeName)); //couteux
        var primaryKeyValue = primaryKey.GetValue(item); // moins couteux

        // alors, oui ca fonctionne, et c'est très bien d'avoir cherché avec les méthodes d'introspection (relexion)
        // c'est un code très générique qui va marchr dans tous les cas
        // sauf si la valeur est nulle, dans ce cas, la ligne ci dessous va péter
        return (string)primaryKeyValue;
        // mais ce n'est pas le problème le plus grave (on va dire que tu es sûr à 100% que la valeur d'un PK est non nulle)

        // le problème majeur c'est la performance
        // appeler la reflexion, que ce soit en .Net ou Java, ca plombe la performance d'execution
        // https://learn.microsoft.com/en-us/archive/msdn-magazine/2005/july/using-net-avoid-common-performance-pitfalls-for-speedier-apps (article assez daté hélas)

        // tu as 2 solutions

        // 1) on garde l'idée de faire appel à la reflexion pour connaitre  la propriété qui est la clé primaire (PropertyInfo)
        // c'est un pointeur, tu peux le stocker dans un dictionnaire , si il est déjà présent, au lieu de refaire la reflexion, juste tu le prends dans le dictionnaire

        // 2) trouver on moyen de ne pas avoir à passer par la reflexion
        // par exemple: toutes les tables auront toujours le même champ qui est la clé primaire, ce champ aura toujours le nom "_id"
        // comme dans MongoDB (pourquoi pas... mais je pense que ca nous servira dans le futur)
    }
}