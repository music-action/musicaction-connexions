CREATE TABLE IF NOT EXISTS answervotes (
    vote_id SERIAL PRIMARY KEY,
    answer_id INTEGER REFERENCES answers(answer_id),
    voter_id INTEGER REFERENCES users(user_id),
    vote_type BOOLEAN NOT NULL
);