CREATE TABLE IF NOT EXISTS products
(
    product_id  SERIAL PRIMARY KEY,
    name        VARCHAR NOT NULL,
    description TEXT
);