CREATE TABLE IF NOT EXISTS questions_produit (
    question_produit_id SERIAL PRIMARY KEY,
    product_reference VARCHAR(255) NOT NULL,
    question_text TEXT NOT NULL,
    user_email VARCHAR(255) NOT NULL,
    question_date TIMESTAMP NOT NULL
)