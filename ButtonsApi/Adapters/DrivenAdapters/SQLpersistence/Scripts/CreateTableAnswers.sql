CREATE TABLE IF NOT EXISTS answers(
    answer_id SERIAL PRIMARY KEY,
    question_id INTEGER REFERENCES questions(question_id),
    content TEXT NOT NULL,
    date_posted TIMESTAMP NOT NULL,
    author_id INTEGER REFERENCES Users(user_id),
    vote_count INTEGER NOT NULL DEFAULT 0
);