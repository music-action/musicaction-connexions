CREATE TABLE IF NOT EXISTS questions (
    question_id SERIAL PRIMARY KEY, 
    product_id INT NOT NULL CONSTRAINT fk_product_id REFERENCES products(product_id), 
    content TEXT, 
    date_posted TIMESTAMP, 
    author_id INT NOT NULL CONSTRAINT fk_author_id REFERENCES users(user_id), 
    vote_count INT);