using System.Linq.Expressions;
using LanguageExt;
using LanguageExt.Common;
using MusicActionApi.Adapters.DrivenAdapters.SQLpersistence.DTO;
using MusicActionApi.HigherAbstractions.General;
using MusicActionApi.HigherAbstractions.Models;

namespace MusicActionApi.Adapters.DrivenAdapters.SQLpersistence;

public class FakePersistenceAdapter<T> : ICanAccessPersistence<T> where T : DtoWithId
{
    private readonly List<T> items = new();

    //TODO: use short ID as a value object with library
    public EitherAsync<Error, int> CreateTable(CancellationToken token)
    {
        return EitherAsync<Error, int>.Right(1);
    }

    public EitherAsync<Error, int> PopulateTable(CancellationToken token)
    {
        var resultatLireTousLesItems = GetAll(token);
        EitherAsync<Error, int> res;
        switch (typeof(T).Name)
        {
            case "BoutonDto":
                res = resultatLireTousLesItems.Bind(lst => InsertSetOfButtons(lst, token));
                break;
            case "QuestionDto":
                res = resultatLireTousLesItems.Bind(lst => InsertSetOfQuestions(lst, token));
                break;
            // Ajoutez d'autres cas pour d'autres types si nécessaire
        }

        return res;
    }

    public EitherAsync<Error, IEnumerable<T>> GetAll(CancellationToken token)
    {
        return EitherAsync<Error, IEnumerable<T>>.Right(items);
    }

    public EitherAsync<Error, T> Insert(T item, CancellationToken token)
    {
        if (items.Any(i => i.Id == item.Id))
            return EitherAsync<Error, T>.Left(
                Error.New("23505: duplicate key value violates unique constraint \"firstkey\""));
        items.Add(item);
        return EitherAsync<Error, T>.Right(item);
    }

    public EitherAsync<Error, T> GetById(string primaryKey, CancellationToken token)
    {
        var item = items.FirstOrDefault(i => i.Id == primaryKey);
        if (item != null)
            return EitherAsync<Error, T>.Right(item);
        return EitherAsync<Error, T>.Left(
            Error.New($"La clé primaire {primaryKey} n'existe pas dans la table {typeof(T).Name}"));
    }

    public EitherAsync<Error, IEnumerable<T>> Get(Expression<Func<T, bool>> predicated, CancellationToken token)
    {
        throw new NotImplementedException();
    }

    public EitherAsync<Error, int> Delete(string primaryKey, CancellationToken token)
    {
        throw new NotImplementedException();
    }

    public EitherAsync<Error, IEnumerable<T>> GetPaging(Expression<Func<T, bool>> predicated, int numberPerPage, int pageNumber, CancellationToken token)
    {
        throw new NotImplementedException();
    }

    public EitherAsync<Error, T> Put(T item, CancellationToken token)
    {
        var itemExistant = GetById(item.Id, token);
        var res = itemExistant.BiBind(
            _ => Update(item, token),
            _ => Insert(item, token));
        return res;
    }

    public EitherAsync<Error, int> Init(CancellationToken token)
    {
        return CreateTable(token).Bind(_ => PopulateTable(token));
    }

    private EitherAsync<Error, int> InsertSetOfButtons(IEnumerable<T> listeBoutons, CancellationToken token)
    {
        if (!listeBoutons.Any())
        {
            var instance = new FakePersistenceAdapter<BoutonDTO>();
            var insert1 = instance.Insert(new BoutonDTO("100", "Occaz", "POST", "/buttons/100/click"), token);
            var insert2 = insert1.Bind(_ =>
                instance.Insert(new BoutonDTO("101", "Aide support", "POST", "/buttons/101/click"), token));
            var insert3 = insert2.Bind(_ =>
                instance.Insert(new BoutonDTO("999", "Occaz Vente", "POST", "/buttons/999/click"), token));
            return EitherAsync<Error, int>.Right(3);
        }

        return EitherAsync<Error, int>.Right(0);
    }

    private EitherAsync<Error, int> InsertSetOfQuestions(IEnumerable<T> listeQuestion, CancellationToken token)
    {
        if (!listeQuestion.Any())
        {
            var instance = new FakePersistenceAdapter<QuestionDTO>();
            var insert1 =
                instance.Insert(
                    new QuestionDTO("12", "ABC123", "Comment jouer de cette guitare ?", "michel@email.com",
                        DateTime.Now, true), token);
            var insert2 = insert1.Bind(_ =>
                instance.Insert(
                    new QuestionDTO("13", "DEF456", "Ce modèle est il le meilleur rapport qualité prix ?",
                        "michel@email.com", DateTime.Now, true), token));
            var insert3 = insert2.Bind(_ =>
                instance.Insert(
                    new QuestionDTO("14", "GHI789", "Comment connecter cette ampli à ma guitare ?", "michel@email.com",
                        DateTime.Now, true), token));
            return EitherAsync<Error, int>.Right(3);
        }

        return EitherAsync<Error, int>.Right(0);
    }

    private EitherAsync<Error, T> Update(T item, CancellationToken token)
    {
        var existingItem = items.First(i => i.Id == item.Id);
        items.Remove(existingItem);
        items.Add(item);
        return EitherAsync<Error, T>.Right(item);
    }
}