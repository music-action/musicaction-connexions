using System.Linq.Expressions;
using LanguageExt;
using LanguageExt.Common;
using MusicActionApi.HigherAbstractions.Models;

namespace MusicActionApi.HigherAbstractions.General;

public interface ICanAccessPersistence<T> where T : DtoWithId
{
    EitherAsync<Error, int> PopulateTable(CancellationToken token);
    EitherAsync<Error, int> CreateTable(CancellationToken token);
    EitherAsync<Error, IEnumerable<T>> GetAll(CancellationToken token);
    EitherAsync<Error, T> Insert(T item, CancellationToken token);
    EitherAsync<Error, T> Put(T item, CancellationToken token);
    EitherAsync<Error, T> GetById(string primaryKey, CancellationToken token);
    EitherAsync<Error, IEnumerable<T>> Get(Expression<Func<T, bool>> predicated, CancellationToken token);
    EitherAsync<Error, int> Delete(string primaryKey, CancellationToken token);
    EitherAsync<Error, IEnumerable<T>> GetPaging(Expression<Func<T, bool>> predicated, int numberPerPage, int pageNumber, CancellationToken token);
    
}