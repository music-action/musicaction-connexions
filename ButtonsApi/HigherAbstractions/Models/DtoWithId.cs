namespace MusicActionApi.HigherAbstractions.Models;

public interface DtoWithId
{
    //TODO: string par un type ULID ou CSLID (et donc qui génere une nouvelle valeur unique à chaque appel)
    string Id { get; }
}