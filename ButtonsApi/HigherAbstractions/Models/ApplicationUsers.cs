using AspNetCore.Identity.MongoDbCore.Models;
using MongoDbGenericRepository.Attributes;

namespace MusicActionApi.HigherAbstractions.Models;

[CollectionName("Users")]
public class ApplicationUsers : MongoIdentityUser<Guid>
{
     
}