using AspNetCore.Identity.MongoDbCore.Models;
using MongoDbGenericRepository.Attributes;

namespace MusicActionApi.HigherAbstractions.Models;

[CollectionName("Roles")]
public class ApplicationRoles : MongoIdentityRole<Guid>
{
    
}