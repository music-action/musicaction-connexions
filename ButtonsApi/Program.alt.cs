using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;

namespace ButtonsApi
{

    // essayer de remettre à niveau avec https://github.com/dotnet/AspNetCore.Docs/blob/main/aspnetcore/tutorials/web-api-help-pages-using-swagger/samples/3.x/TodoApi.Swashbuckle/Program.cs
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup2>();
    }
}