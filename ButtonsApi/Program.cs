using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using MusicActionApi.Adapters.DrivenAdapters.MongoDBPersistence;
using MusicActionApi.Adapters.DrivenAdapters.SQLpersistence.DTO;

using MusicActionApi.HigherAbstractions.General;
using MusicActionApi.HigherAbstractions.Models;
using Serilog;

[assembly: ApiController]

var builder = WebApplication.CreateBuilder(args);

builder.Configuration.AddJsonFile("appsettings.json")
    .AddJsonFile($"appsettings.{builder.Environment.EnvironmentName}.json", true)
    .AddEnvironmentVariables("docker_");

// New code to configure Serilog
var logger = new LoggerConfiguration()
    .ReadFrom.Configuration(builder.Configuration)
    .Enrich.FromLogContext()
    .CreateLogger();

builder.Logging.ClearProviders();
builder.Logging.AddSerilog(logger);

builder.Services.AddControllers();

var configuration = builder.Configuration;

var connectionString = configuration.GetValue<string>("ConnectionStrings:MongoDB");

builder.Services.AddIdentity<ApplicationUsers, ApplicationRoles>()
    .AddMongoDbStores<ApplicationUsers, ApplicationRoles, Guid>(
        connectionString, "Identity"
    );

//TODO : Configure JWT values for Issuer, Audience and Key
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateLifetime = true,
            ValidateIssuerSigningKey = true,
            ValidIssuer = configuration["Jwt:Issuer"],
            ValidAudience = configuration["Jwt:Audience"],
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:Key"]))
        };
    });


//TODO: à changer pour mongodb, les controllers etc
builder.Services.AddHealthChecks()
    .AddNpgSql(
        connectionString!,
        name: "PostgreSQL",
        failureStatus: HealthStatus.Unhealthy
    );

/*builder.Services.AddSingleton(new DbConnectionProvider(connectionString!));*/
builder.Services.AddSingleton(new MongoDBConnection(connectionString!, "product_interactions"));

/*builder.Services.AddScoped<ICanAccessPersistence<BoutonDTO>, PostGreSqlAdapter<BoutonDTO>>();
builder.Services.AddScoped<ICanAccessPersistence<QuestionDTO>, PostGreSqlAdapter<QuestionDTO>>();*/
builder.Services.AddScoped<ICanAccessPersistence<BoutonDTO>, MongoDBAdapter<BoutonDTO>>();
builder.Services.AddScoped<ICanAccessPersistence<QuestionDTO>, MongoDBAdapter<QuestionDTO>>();


builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "Music Action API", Version = "v1" });
});

var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
builder.Services.AddCors(options =>
{
    options.AddPolicy(MyAllowSpecificOrigins,
        policy =>
        {
            policy.WithOrigins("*")
                .AllowAnyHeader()
                .AllowAnyMethod();
        });
});

var app = builder.Build();
const string startMessage = "Starting the Web application";
const string connectionStringMessage = "Using following connection string : {ConnectionString}";

app.Logger.LogInformation(startMessage);
app.Logger.LogInformation(connectionStringMessage, connectionString);

if (app.Environment.IsProduction()) app.UseHttpsRedirection();

app.UseCors(MyAllowSpecificOrigins);
app.UseAuthentication();
app.UseAuthorization();

app.UseSwagger();
app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "Music Action API V1"); });
app.MapControllers();
app.MapHealthChecks("/health");

app.Run();


namespace MusicActionApi
{
    public partial class Program
    {
    }
}