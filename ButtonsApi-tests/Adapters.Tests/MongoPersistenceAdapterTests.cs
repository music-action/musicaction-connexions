using FluentAssertions;
using MusicActionApi.Adapters.DrivenAdapters.MongoDBPersistence;
using MusicActionApi.Adapters.DrivenAdapters.SQLpersistence.DTO;
using MusicActionApi.HigherAbstractions.General;
using Testcontainers.MongoDb;

// https://github.com/testcontainers/testcontainers-dotnet

namespace MusicActionApi.Tests.Adapters.Tests;

// ATTENTION: avec XUnit, la classe de test est détruite puis re créé à chaque "Fact"
// https://hovermind.com/xunit/setup-and-teardown.html
public class MongoPersistenceAdapterTests : IAsyncLifetime
{
    private readonly MongoDbContainer _mongoContainer;
    protected Lazy<ICanAccessPersistence<BoutonDTO>> _persistenceAdapterLazyForBouton;
    protected Lazy<ICanAccessPersistence<QuestionDTO>> _persistenceAdapterLazyForQuestion;

    // ReSharper disable once ConvertConstructorToMemberInitializers
    public MongoPersistenceAdapterTests()
    {
        _mongoContainer = new MongoDbBuilder()
                .WithImage("mongo:6.0")
            .WithCleanUp(true)
            .Build();
        
        _persistenceAdapterLazyForBouton = new Lazy<ICanAccessPersistence<BoutonDTO>>(() =>
          new MongoDBAdapter<BoutonDTO>(
              new MongoDBConnection(_mongoContainer.GetConnectionString(), "buttons"))
        );
        _persistenceAdapterLazyForQuestion = new Lazy<ICanAccessPersistence<QuestionDTO>>(() =>
          new MongoDBAdapter<QuestionDTO>(
              new MongoDBConnection(_mongoContainer.GetConnectionString(), "questions"))
        );
        
    }
    public Task InitializeAsync()
    {
           return _mongoContainer.StartAsync();
    }

    public Task DisposeAsync()
    {   
             return _mongoContainer.DisposeAsync().AsTask();
    }

    [Fact]
    public async Task GetByIdUnElement_Succés()
    {
        _persistenceAdapterLazyForBouton.Should().NotBeNull();
        // Given
        var underTestAdapter = _persistenceAdapterLazyForBouton.Value;
        // When
        var id1 = Ulid.NewUlid().ToString();
        await underTestAdapter.Insert(new BoutonDTO(Ulid.NewUlid().ToString(), "Aide", "GET", "/buttons/241/click"), CancellationToken.None);
        await underTestAdapter.Insert(new BoutonDTO(id1, "Occaz", "GET", "/buttons/864/click"), CancellationToken.None);
        var eitherGetAll = await underTestAdapter.GetById(id1, CancellationToken.None);
        // Then
        var resultatDuMatch = eitherGetAll.Match(
            valeur => valeur.buttonText.Should().Be("Occaz"),
            ex =>  Assert.Fail(ex.Message));
    }
    
    [Fact]
    public async Task GetByIdAvecUneCléPrimaireQuiNestPasUnULID_Echec()
    {
        _persistenceAdapterLazyForBouton.Should().NotBeNull();
        // Given
        var underTestAdapter = _persistenceAdapterLazyForBouton.Value;
        // When
        await underTestAdapter.Insert(new BoutonDTO(Ulid.NewUlid().ToString(), "Aide", "GET", "/buttons/241/click"), CancellationToken.None);
        await underTestAdapter.Insert(new BoutonDTO(Ulid.NewUlid().ToString(), "Occaz", "GET", "/buttons/864/click"), CancellationToken.None);
        var eitherGetAll = await underTestAdapter.GetById("999", CancellationToken.None);
        // Then
        var resultatDuMatch = eitherGetAll.Match(
            valeur => valeur.Should().BeNull("on ne devrait pas trouver ce bouton"),
            ex => ex.Message.Should().Be("L'identifiant 999 n'est pas un ULID valide"));
    }
    
    [Fact]
    public async Task GetByIdAvecUneCléPrimaireQuiNestPasDansLaBD_Echec()
    {
        _persistenceAdapterLazyForBouton.Should().NotBeNull();
        // Given
        var underTestAdapter = _persistenceAdapterLazyForBouton.Value;
        // When
        var id1 = Ulid.NewUlid().ToString();
        await underTestAdapter.Insert(new BoutonDTO(Ulid.NewUlid().ToString(), "Aide", "GET", "/buttons/241/click"), CancellationToken.None);
        await underTestAdapter.Insert(new BoutonDTO(Ulid.NewUlid().ToString(), "Occaz", "GET", "/buttons/864/click"), CancellationToken.None);
        var eitherGetAll = await underTestAdapter.GetById(id1, CancellationToken.None);
        // Then
        var resultatDuMatch = eitherGetAll.Match(
            valeur => valeur.Should().BeNull("on ne devrait pas trouver ce bouton"),
            ex => ex.Message.Should().Be($"L'identifiant {id1} n'existe pas dans la collection BoutonDTO"));
    }
    
    [Fact]
    public async Task InsertDeuxBoutons_Succés()
    {
        _persistenceAdapterLazyForBouton.Should().NotBeNull();
        // Given
        var underTestAdapter = _persistenceAdapterLazyForBouton.Value;
        // When
        await underTestAdapter.Insert(new BoutonDTO(Ulid.NewUlid().ToString(), "Aide", "GET", "/buttons/241/click"), CancellationToken.None);
        await underTestAdapter.Insert(new BoutonDTO(Ulid.NewUlid().ToString(), "Occaz", "GET", "/buttons/864/click"), CancellationToken.None);
        var eitherGetAll = await underTestAdapter.GetAll(CancellationToken.None);
        // Then
        var r = eitherGetAll.Match(
            valeur => valeur.Should().HaveCount(2),
            ex => Assert.Fail(ex.Message));
    }
    
    [Fact]
    public async Task InsertUnBoutonAvecUnIdInvalide_Echec()
    {
        _persistenceAdapterLazyForBouton.Should().NotBeNull();
        // Given
        var underTestAdapter = _persistenceAdapterLazyForBouton.Value;
        // When
        var eitherGetAll = await underTestAdapter.Insert(new BoutonDTO("123", "Aide", "GET", "/buttons/241/click"), CancellationToken.None);
        // Then
        var resultatDuMatch = eitherGetAll.Match(
            valeur => valeur.Should().BeNull("on devrait avoir une erreur ici"),
            ex => ex.Message.Should().Be($"L'identifiant 123 n'est pas un ULID valide"));
    }
    
    [Fact]
    public async Task InsertUnBoutonQuiExisteDeja_Echec()
    {
        _persistenceAdapterLazyForBouton.Should().NotBeNull();
        // Given
        var underTestAdapter = _persistenceAdapterLazyForBouton.Value;
        // When
        var id1 = Ulid.NewUlid().ToString();
        await underTestAdapter.Insert(new BoutonDTO(id1, "Aide", "GET", "/buttons/241/click"), CancellationToken.None);
        var eitherGetAll = await underTestAdapter.Insert(new BoutonDTO(id1, "Occaz", "GET", "/buttons/864/click"), CancellationToken.None);
        // Then
        var resultatDuMatch = eitherGetAll.Match(
            valeur => valeur.Should().BeNull("on ne devrait pas pouvoir insérer un nouveau bouton avec un id déjà existant"),
            ex => ex.Message.Should().Be($"Un élément avec l'identifiant {id1} existe déjà"));
    }
    
    [Fact]
    public async Task PutUneDonneeBoutonQuiNExistePasDeja_Echec()
    {
        _persistenceAdapterLazyForBouton.Should().NotBeNull();
        // Given
        var underTestAdapter = _persistenceAdapterLazyForBouton.Value;
        // When
        var id1 = Ulid.NewUlid().ToString();
        var resultatPut = await underTestAdapter.Put(new BoutonDTO(id1, "Support Client", "GET", "/buttons/864/click"), CancellationToken.None);
        // Then
        var resultatDuMatch = resultatPut.Match(
            valeur => valeur.Should().BeNull("On ne devrait pas pouvoir insérer un nouveau bouton avec Put"),
            ex => ex.Message.Should().Be($"L'identifiant {id1} n'existe pas dans la collection BoutonDTO"));
    }

    [Fact]
    public async Task PutUneDonneeBouton_Succés()
    {
        _persistenceAdapterLazyForBouton.Should().NotBeNull();
        // Given
        var underTestAdapter = _persistenceAdapterLazyForBouton.Value;
        // When
        var id1 = Ulid.NewUlid().ToString();
        var _1 = await underTestAdapter.Insert(new BoutonDTO(id1, "SAV", "POST", "/buttons/864/click"), CancellationToken.None);
        var resultatPut = await underTestAdapter.Put(new BoutonDTO(id1, "Vente Occaz", "POST", "buttons/864/click"), CancellationToken.None);
        var eitherGetAll = await underTestAdapter.GetById(id1, CancellationToken.None);
        // Then
        var resultatDuMatch = eitherGetAll.Match(
            valeur => valeur.buttonText.Should().Be("Vente Occaz"),
            ex => ex.Should().Be(null));
    }
    
    [Fact]
    public async Task GetUneDonneeBoutonParPredicat_Succés()
    {
        _persistenceAdapterLazyForBouton.Should().NotBeNull();
        // Given
        var underTestAdapter = _persistenceAdapterLazyForBouton.Value;
        // When
        await underTestAdapter.Insert(new BoutonDTO(Ulid.NewUlid().ToString(), "Aide", "GET", "/buttons/241/click"), CancellationToken.None);
        await underTestAdapter.Insert(new BoutonDTO(Ulid.NewUlid().ToString(), "Occaz", "GET", "/buttons/864/click"), CancellationToken.None);
        var eitherGetAll = await underTestAdapter.Get(x => x.buttonText == "Occaz", CancellationToken.None);
        // Then
        var resultatDuMatch = eitherGetAll.Match(
            valeur => valeur.Should().HaveCount(1),
            ex => Assert.Fail(ex.Message));
    }
    
    [Fact]
    public async Task GetUneDonneeBoutonQuiNexistePasParPredicat_Succés()
    {
        _persistenceAdapterLazyForBouton.Should().NotBeNull();
        // Given
        var underTestAdapter = _persistenceAdapterLazyForBouton.Value;
        // When
        await underTestAdapter.Insert(new BoutonDTO(Ulid.NewUlid().ToString(), "Aide", "GET", "/buttons/241/click"), CancellationToken.None);
        await underTestAdapter.Insert(new BoutonDTO(Ulid.NewUlid().ToString(), "Occaz", "GET", "/buttons/864/click"), CancellationToken.None);
        var eitherGetAll = await underTestAdapter.Get(x => x.buttonText == "Acheter", CancellationToken.None);
        // Then
        var resultatDuMatch = eitherGetAll.Match(
            valeur => valeur.Should().HaveCount(0),
            ex => Assert.Fail(ex.Message));
    }
    
    [Fact]
    public async Task SupprimerUnBouton_Succés()
    {
        _persistenceAdapterLazyForBouton.Should().NotBeNull();
        // Given
        var underTestAdapter = _persistenceAdapterLazyForBouton.Value;
        // When
        var id1 = Ulid.NewUlid().ToString();
        await underTestAdapter.Insert(new BoutonDTO(id1, "Aide", "GET", "/buttons/241/click"), CancellationToken.None);
        await underTestAdapter.Insert(new BoutonDTO(Ulid.NewUlid().ToString(), "Occaz", "GET", "/buttons/864/click"), CancellationToken.None);
        await underTestAdapter.Delete(id1, CancellationToken.None);
        var eitherGetAll = await underTestAdapter.GetAll(CancellationToken.None);
        // Then
        var r = eitherGetAll.Match(
            valeur => valeur.Should().HaveCount(1),
            ex => Assert.Fail(ex.Message));
    }
    
    //Test pour la généricité (utilisation d'un autre type)
    [Fact]
    public async Task InsertDeuxQuestions_Succés()
    {
        _persistenceAdapterLazyForQuestion.Should().NotBeNull();
        // Given
        var underTestAdapter = _persistenceAdapterLazyForQuestion.Value;
        // When
        await underTestAdapter.Insert(new QuestionDTO(Ulid.NewUlid().ToString(), "ABC123", "Comment jouer de cette guitare ?", "michel@email.com", DateTime.Now, true), CancellationToken.None);
        await underTestAdapter.Insert(new QuestionDTO(Ulid.NewUlid().ToString(), "DEF456", "Ce modèle est il le meilleur rapport qualité prix ?", "michel@email.com", DateTime.Now, true), CancellationToken.None);
        var eitherGetAll = await underTestAdapter.GetAll(CancellationToken.None);
        // Then
        var r = eitherGetAll.Match(
            valeur => valeur.Should().HaveCount(2),
            ex => Assert.Fail(ex.Message));
    }

    [Fact]
    public async Task PutUneDonneeQuestion_Succés()
    {
        _persistenceAdapterLazyForQuestion.Should().NotBeNull();
        // Given
        var underTestAdapter = _persistenceAdapterLazyForQuestion.Value;
        // When
        var id1 = Ulid.NewUlid().ToString();
        var _1 = await underTestAdapter.Insert(new QuestionDTO(id1, "ABC123", "Comment jouer de cette guitare ?", "michel@email.com", DateTime.Now, true), CancellationToken.None);
        var resultatPut = await underTestAdapter.Put(new QuestionDTO(id1, "DEF456", "Ce modèle est il le meilleur rapport qualité prix ?", "michel@email.com", DateTime.Now, true), CancellationToken.None);
        var eitherGetAll = await underTestAdapter.GetById(id1, CancellationToken.None);
        // Then
        var resultatDuMatch = eitherGetAll.Match(
            valeur => valeur.productReference.Should().Be("DEF456"),
            ex => ex.Should().Be(null));
    }
}


    // si on veut faire des tests avec une base de données qui se remplit "par défaut" à chaque lancement de test
    // il faut utiliser le mécanisme de "migration" qui est prévu dans la librairie TestContainer
    // voir  https://github.com/testcontainers/testcontainers-dotnet/tree/develop/examples/Flyway/tests/Flyway.Tests/migrate
