/*
using ButtonsApi.Adapters.DrivenAdapters.SQLpersistence.DTO;
using ButtonsApi.HigherAbstractions;
using ButtonsApi.Services.DrivenAdapters.SQLpersistence;
using FluentAssertions;
using Testcontainers.PostgreSql;

// https://github.com/testcontainers/testcontainers-dotnet

namespace ButtonsApi_tests.Adapters.Tests;

// ATTENTION: avec XUnit, la classe de test est détruite puis re créé à chaque "Fact"
// https://hovermind.com/xunit/setup-and-teardown.html
public class PgSqlPersistenceAdapterTests : IAsyncLifetime
{
    private readonly PostgreSqlContainer _postgresContainer;
    protected Lazy<ICanAccessPersistence<BoutonDTO>> _persistenceAdapterLazyForBouton;
    protected Lazy<ICanAccessPersistence<QuestionDTO>> _persistenceAdapterLazyForQuestion;

    // ReSharper disable once ConvertConstructorToMemberInitializers
    public PgSqlPersistenceAdapterTests()
    {
        _postgresContainer = new PostgreSqlBuilder()
            .WithImage("postgres:15-alpine")
            .WithCleanUp(true)
            .Build();
        
        _persistenceAdapterLazyForBouton = new Lazy<ICanAccessPersistence<BoutonDTO>>(() =>
          new PostGreSqlAdapter<BoutonDTO>(
              new DbConnectionProvider(_postgresContainer.GetConnectionString()))
        );
        
        _persistenceAdapterLazyForQuestion = new Lazy<ICanAccessPersistence<QuestionDTO>>(() =>
          new PostGreSqlAdapter<QuestionDTO>(
              new DbConnectionProvider(_postgresContainer.GetConnectionString()))
        );
    }

    public Task InitializeAsync()
    {
           return _postgresContainer.StartAsync();
    }

    public Task DisposeAsync()
    {   
             return _postgresContainer.DisposeAsync().AsTask();
    }
    
    [Fact]
    public async Task CreateTable_Succés()
    {
        _persistenceAdapterLazyForBouton.Should().NotBeNull();
        // Given
        var underTestAdapter = _persistenceAdapterLazyForBouton.Value;
        // When  
        await underTestAdapter.CreateTable(CancellationToken.None);
        // And When
        var resMonad = await underTestAdapter
            .GetAll(CancellationToken.None);
        // Then
        var resultatDuMatch = resMonad.Match(
            valeur => valeur.Should().BeEmpty(),
            ex => Assert.Fail(ex.Message));
    }
    
    [Fact]
    public async Task InsertDeuxBoutons_Succés()
    {
        _persistenceAdapterLazyForBouton.Should().NotBeNull();
        // Given
        var underTestAdapter = _persistenceAdapterLazyForBouton.Value;
        var resCreate = await underTestAdapter.CreateTable(CancellationToken.None).Match(x => x,
            ex => throw new Exception("not good"));
        // When
        await underTestAdapter.Insert(new BoutonDTO("10", "Aide", "GET", "/buttons/241/click"), CancellationToken.None);
        await underTestAdapter.Insert(new BoutonDTO("11", "Occaz", "GET", "/buttons/864/click"), CancellationToken.None);
        var eitherGetAll = await underTestAdapter.GetAll(CancellationToken.None);
        // Then
        var r = eitherGetAll.Match(
            valeur => valeur.Should().HaveCount(2),
            ex => Assert.Fail(ex.Message));
    }

    [Fact]
    public async Task InsertBoutonAvecIdDejaDansLaBD_Echec()
    {
        _persistenceAdapterLazyForBouton.Should().NotBeNull();
        // Given
        var underTestAdapter = _persistenceAdapterLazyForBouton.Value;
        await underTestAdapter.CreateTable(CancellationToken.None);
        await underTestAdapter.Insert(new BoutonDTO("241", "Aide", "GET", "/buttons/241/click"), CancellationToken.None);
        // When
        var resMonad = await underTestAdapter
            .Insert(new BoutonDTO("241", "Occaz", "GET", "/buttons/241/click"), CancellationToken.None);
        // Then
        var resultatDuMatch = resMonad.Match(
            valeur => { Assert.Fail("on devrait avoir une exception ici"); },
            ex => { ex.Message.Should().StartWith("23505: duplicate key value violates unique constraint"); });
    }
   

    [Fact]
    public async Task GetById_Succés()
    {
        _persistenceAdapterLazyForBouton.Should().NotBeNull();
        // Given
        var underTestAdapter = _persistenceAdapterLazyForBouton.Value;
        await underTestAdapter.CreateTable(CancellationToken.None);
        // When
        await underTestAdapter.Insert(new BoutonDTO("12", "Aide", "GET", "/buttons/241/click"), CancellationToken.None);
        await underTestAdapter.Insert(new BoutonDTO("13", "Occaz", "GET", "/buttons/864/click"), CancellationToken.None);
        var eitherGetAll = await underTestAdapter.GetById("13", CancellationToken.None);
        // Then
        var resultatDuMatch = eitherGetAll.Match(
            valeur => valeur.buttonText.Should().Be("Occaz"),
            ex =>  Assert.Fail(ex.Message));
    }
    
    [Fact]
    public async Task GetByIdLaClePrimaireNexistePas_Echec()
    {
        _persistenceAdapterLazyForBouton.Should().NotBeNull();
        // Given
        var underTestAdapter = _persistenceAdapterLazyForBouton.Value;
        await underTestAdapter.CreateTable(CancellationToken.None);
        // When
        await underTestAdapter.Insert(new BoutonDTO("12", "Aide", "GET", "/buttons/241/click"), CancellationToken.None);
        await underTestAdapter.Insert(new BoutonDTO("13", "Occaz", "GET", "/buttons/864/click"), CancellationToken.None);
        var eitherGetAll = await underTestAdapter.GetById("14", CancellationToken.None);
        // Then
        var resultatDuMatch = eitherGetAll.Match(
            valeur => { Assert.Fail("on devrait avoir une exception ici"); },
            ex =>  { ex.Message.Should().StartWith("La clé primaire 14 n'existe pas dans la table BoutonDTO"); });
    }

    [Fact]
    public async Task PutUneDonneeBoutonQuiNExistePasDeja_Succés()
    {
        _persistenceAdapterLazyForBouton.Should().NotBeNull();
        // Given
        var underTestAdapter = _persistenceAdapterLazyForBouton.Value;
        await underTestAdapter.CreateTable(CancellationToken.None);
        // When
        var resultatPut = await underTestAdapter.Put(new BoutonDTO("10", "Support Client", "GET", "/buttons/864/click"), CancellationToken.None);
        // Then
        var resultatDuMatch = resultatPut.Match(
            valeur => valeur.buttonText.Should().Be("Support Client"),
            ex =>  Assert.Fail(ex.Message));
    }

    [Fact]
    public async Task PutUneDonneeBoutonQuiExisteDeja_Succés()
    {
        _persistenceAdapterLazyForBouton.Should().NotBeNull();
        // Given
        var underTestAdapter = _persistenceAdapterLazyForBouton.Value;
        await underTestAdapter.CreateTable(CancellationToken.None);
        // When
        var _1 = await underTestAdapter.Put(new BoutonDTO("11", "SAV", "POST", "/buttons/864/click"), CancellationToken.None);
        var resultatPut = await underTestAdapter.Put(new BoutonDTO("11", "Vente Occaz", "POST", "buttons/864/click"), CancellationToken.None);
        var eitherGetAll = await underTestAdapter.GetById("11", CancellationToken.None);
        // Then
        var resultatDuMatch = eitherGetAll.Match(
            valeur => valeur.buttonText.Should().Be("Vente Occaz"),
            ex => ex.Should().Be(null));
    }
    
    //Test pour la généricité (utilisation d'un autre type)
    [Fact]
    public async Task InsertDeuxQuestions_Succés()
    {
        _persistenceAdapterLazyForQuestion.Should().NotBeNull();
        // Given
        var underTestAdapter = _persistenceAdapterLazyForQuestion.Value;
        await underTestAdapter.CreateTable(CancellationToken.None);
        // When
        await underTestAdapter.Insert(new QuestionDTO("11", "ABC123", "Comment jouer de cette guitare ?", "michel@email.com", DateTime.Now), CancellationToken.None);
        await underTestAdapter.Insert(new QuestionDTO("12", "DEF456", "Ce modèle est il le meilleur rapport qualité prix ?", "michel@email.com", DateTime.Now), CancellationToken.None);
        var eitherGetAll = await underTestAdapter.GetById("11", CancellationToken.None);
        // Then
        var resultatDuMatch = eitherGetAll.Match(
            valeur => valeur.productReference.Should().Be("ABC123"),
            ex => Assert.Fail(ex.Message));
    }

    [Fact]
    public async Task PutUneDonneeQuestion_Succés()
    {
        _persistenceAdapterLazyForQuestion.Should().NotBeNull();
        // Given
        var underTestAdapter = _persistenceAdapterLazyForQuestion.Value;
        await underTestAdapter.CreateTable(CancellationToken.None);
        // When
        var _1 = await underTestAdapter.Put(new QuestionDTO("11", "ABC123", "Comment jouer de cette guitare ?", "michel@email.com", DateTime.Now), CancellationToken.None);
        var resultatPut = await underTestAdapter.Put(new QuestionDTO("11", "DEF456", "Ce modèle est il le meilleur rapport qualité prix ?", "michel@email.com", DateTime.Now), CancellationToken.None);
        var eitherGetAll = await underTestAdapter.GetById("11", CancellationToken.None);
        // Then
        var resultatDuMatch = eitherGetAll.Match(
            valeur => valeur.productReference.Should().Be("DEF456"),
            ex => ex.Should().Be(null));
    }
}

    // si on veut faire des tests avec une base de données qui se remplit "par défaut" à chaque lancement de test
    // il faut utiliser le mécanisme de "migration" qui est prévu dans la librairie TestContainer
    // voir  https://github.com/testcontainers/testcontainers-dotnet/tree/develop/examples/Flyway/tests/Flyway.Tests/migrate
*/
