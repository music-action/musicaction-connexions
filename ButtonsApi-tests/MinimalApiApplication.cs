using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace MusicActionApi.Tests;

public  class MinimalApiApplication : WebApplicationFactory<Program>
{
    private readonly Action<IServiceCollection> _serviceInjection;
    private readonly string _environment;

    public MinimalApiApplication(
        Action<IServiceCollection> serviceInjection ,
        string environment = "development"
        )
    {
        _serviceInjection = serviceInjection; 
        _environment = environment;
    }

    protected override IHost CreateHost(IHostBuilder builder)
    {
        _ = builder.UseEnvironment(_environment);
        return base.CreateHost(builder);
    }
    
    protected override void ConfigureWebHost(IWebHostBuilder builder)
    { 
        builder.ConfigureServices(_serviceInjection);
    }   
}

