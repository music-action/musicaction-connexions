using Microsoft.Extensions.Logging;
using MusicActionApi.Adapters.DriverAdapters.RestAPI.Controllers;
using Microsoft.Extensions.Logging.Console;

namespace MusicActionApi.Tests.Fixtures
{
    public class LoggerFixture
    {
        public ILogger<ButtonsController> Logger { get; }

        public LoggerFixture()
        {
            var loggerFactory = LoggerFactory.Create(builder =>
            {
                builder.AddConsole();
            });

            Logger = loggerFactory.CreateLogger<ButtonsController>();
        }
    }
}