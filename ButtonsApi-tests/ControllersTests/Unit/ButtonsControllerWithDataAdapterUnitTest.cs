using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MusicActionApi.Adapters.DrivenAdapters.SQLpersistence;
using MusicActionApi.Adapters.DrivenAdapters.SQLpersistence.DTO;
using MusicActionApi.Adapters.DriverAdapters.RestAPI.Controllers;
using MusicActionApi.Adapters.DriverAdapters.RestAPI.Models;
using MusicActionApi.HigherAbstractions.General;
using MusicActionApi.Tests.Fixtures;

namespace MusicActionApi.Tests.ControllersTests.Unit;
#pragma warning disable CS8604
public class ButtonsControllerWithDataAdapterUnitTest : IClassFixture<LoggerFixture>
{
    private readonly ButtonsController _controller;
    private readonly ICanAccessPersistence<BoutonDTO> _adapter;

    public ButtonsControllerWithDataAdapterUnitTest(LoggerFixture fixture)
    {
        var logger = fixture.Logger;
        _adapter = new FakePersistenceAdapter<BoutonDTO>();
        _controller = new ButtonsController(_adapter, logger);
    }

    // ... rest of your test methods

    [Fact]
    public async Task Get_WhenCalled_ReturnsOkResult()
    {
        //Arrange
        _adapter.Insert(new BoutonDTO("241", "Aide", "GET", "/buttons/241/click"), CancellationToken.None);

        // Act
        var okResult = await _controller.GetAll(CancellationToken.None);

        // Assert
        var okObjectResult = okResult.Should().BeOfType<OkObjectResult>().Subject;
        var listButtons = okObjectResult.Value.Should().BeAssignableTo<IEnumerable<BoutonViewDto>>().Subject;
        const int NbElementsInList = 1;

        listButtons.Should().HaveCount(NbElementsInList);

        // je dois remettre sur pied le mapping qui calculait la BaseRoute automatiquement

        // listButtons.Last().ActionRoute.Should().EndWith(NbElementsInList.ToString());
/*
    listButtons.First().ActionRoute.StartsWith(ButtonsController.BaseRoute).Should()
        .BeTrue($"the string is '{listButtons.Last().ActionRoute}'");
    listButtons.Last().ActionRoute.StartsWith(ButtonsController.BaseRoute).Should()
        .BeTrue($"the string is '{listButtons.Last().ActionRoute}'");

    listButtons.Last().ActionRoute.EndsWith("test2")
        .Should().BeTrue($"the string is '{listButtons.Last().ActionRoute}'");
        */
    }

#pragma warning restore CS8604
}