﻿using System.Diagnostics;
using System.Net;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using MusicActionApi.Adapters.DrivenAdapters.SQLpersistence;
using MusicActionApi.Adapters.DrivenAdapters.SQLpersistence.DTO;
using MusicActionApi.Adapters.DriverAdapters.RestAPI.Models;
using MusicActionApi.HigherAbstractions.General;
using NSubstitute;
using Swashbuckle.AspNetCore.Swagger;
using Xunit.Abstractions;

namespace MusicActionApi.Tests.ControllersTests.Integration
{/// <summary>
 /// integration tests for a web application.
 /// It uses the WebApplicationFactory framework to create a test server that runs the application under test.
 /// It allows to perform HTTP requests and assert the results against JSON deserialization.
 /// It also uses the FluentAssertions library to assert the results of the tests.
 /// </summary>
    public sealed class WebApiIntegrationTests : IDisposable
    {
        private readonly ITestOutputHelper _testOutputHelper;

        // this will construct the WebApp with default configuration (no fakes)
        private MinimalApiApplication? _application = null;
        private readonly ICanAccessPersistence<BoutonDTO> _adapter;
      //  

        public WebApiIntegrationTests(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
            _adapter = new FakePersistenceAdapter<BoutonDTO>();
        }
        
        [Fact(Skip = "security tokens are not correctly in place")]
        [Trait("Category","Integration")]
        [Trait("Environment","Production")]
        public void CheckingRouteWithSecurityTokens()
        {
            //this._application = new((_) => { },"integration");
            //// Arrange
            //using var httpClient = this._application.CreateClient();
            //var token = new JWTTokenUtility().GenerateTestToken(); // our token generation logic
            //var stringToken = new JwtSecurityTokenHandler().WriteToken(token);
            //// Attach the token to the request headers
            //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", stringToken);

            //  // Act
            //using var response = await httpClient.GetAsync("/hello");
            // response.StatusCode.Should().Be(HttpStatusCode.OK  );
          
            // var result = (await response.Content.ReadAsStringAsync()).Deserialize<List<String>>();

            //// Assert
            //result.Should().BeInAscendingOrder();
            //Debug.Assert(result != null, nameof(result) + " != null");
            //result.First().Should().Be("Hello");
            //result.Last().Should().Be("ITER");
        }
 
        [Fact()]
        public async Task CheckHelloRoute()
        {
            // Arrange
            _application = new((_) => { },"production");  
            using var httpClient = _application.CreateClient();
            // simulation for CORS
            httpClient.DefaultRequestHeaders.Add("Origin", "http://foo.com");
            // Act
            using var response = await httpClient.GetAsync("/api/buttons/hello" ); 
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK  );
            response.Headers.Should().ContainKey("Access-Control-Allow-Origin");
            //TODO: verify which domain is allowed      
        }
       
        [Fact]
        public async Task CheckingHealthWithRealWebAppAndFakeService()
        {   
            // mock a fake service
            var mockedService = Substitute.For<ICanAccessPersistence<BoutonDTO>>();
            mockedService.GetAll(Arg.Any<CancellationToken>()).Returns( new List<BoutonDTO>{ new BoutonDTO("test", "ciao", "GET", "/buttons/test/click")} );
            // Arrange
            _application = new(s => { s.AddSingleton(mockedService); });  
            using var httpClient = _application.CreateClient();
            // Act
            using var response = await httpClient.GetAsync("/api/buttons"); 
          
            // Permet d'inspecter le contenu de la réponse, on voit qu'il y a un problème en debuggant
            var rawJsonResponse = await response.Content.ReadAsStringAsync();
            _testOutputHelper.WriteLine(rawJsonResponse);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK  );
            // this kind of test is also important to check if serialization is acting as expected !!!
            var result = (await response.Content.ReadAsStringAsync()).Deserialize<List<BoutonViewDto>>();
          
            result.Last().ActionRoute.Should().StartWith("api/buttons/");
        }
        
        //NICE_TO_HAVE: add a test that verify what happens with a cancellation token
        
        //NICE_TO_HAVE: add a test that launch the webApp with a failing database and verifiy that the error is properly handled
        //handled (no 500 error) and that someting is logged, with any known route 
        // (so easy to do with NSubstitute)
        
        [Fact]
        public void SwaggerInOperationTest()
        {
            //  act
            this._application = new((_) => { },"development");
            var swagger = this._application.Services.GetRequiredService<ISwaggerProvider>()
             .GetSwagger("v1");
            // assert
            swagger.Should().NotBeNull();
            swagger.Paths.Any().Should().BeTrue();

            swagger.Components.Schemas.Should().NotBeNull();
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing && _application != null)
            {
                this._application.Dispose();
                this._application = null!;
            }
        }
       
    }
}


   /*
        [Fact]
        [Trait("Category","Integration")]
        [Trait("Environment","Development")]
        //https://stackoverflow.com/questions/42262651/how-do-you-filter-xunit-tests-by-trait-with-dotnet-test
        public async Task CheckHelloRouteWithException_OrdinaryError_InDev()
        {
            // Arrange
            var _mockedLogger = Substitute.For<ILogger<Program>>();   // on devrait avoir un GlobalExceptionHandler à un moment
            _application = new(
                s => s.AddSingleton(_mockedLogger), 
                "development");
            
            WebApplicationFactoryClientOptions options = new WebApplicationFactoryClientOptions();
            using var httpClient = _application.CreateClient(options);

            // Act
            using var response = await httpClient.GetAsync("/pbs/hello?exceptionWanted=1"); 
           
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.InternalServerError  );
            var result =   await response.Content.ReadAsStringAsync();
            result.Should().StartWith(
                "System.ArgumentException: a test internal Error");
            //nothing in the logger when we are in Dev mode
            _mockedLogger.Received(0)
                .Log(Arg.Is(LogLevel.Error),
                    Arg.Is<EventId>(0),
                    Arg.Is<object>(x =>  x.ToString() == "Exception occurred: a test internal Error"),
                    Arg.Any<Exception>(),
                    Arg.Any<Func<object, Exception?, string>>()
                );

        }
     
        [Fact]
        [Trait("Category","Integration")]
        [Trait("Environment","Production")]
        public async Task CheckHelloRouteWithException_GlobalExceptionHandler_InProd()
        {
            // Arrange
            var _mockedLogger = Substitute.For<ILogger<Program>>();
            _application = new(
                s => s.AddSingleton(_mockedLogger), 
                "production");
            
            WebApplicationFactoryClientOptions options = new WebApplicationFactoryClientOptions();
            using var httpClient = _application.CreateClient(options);

            // Act
            using var response = await httpClient.GetAsync("/pbs/hello?exceptionWanted=1"); 
           
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotExtended  );
            var result =   await response.Content.ReadAsStringAsync();
            result.Should().StartWith(
                @"{""status"":510,""title"":""ICP4 cannot process your resquest""");

            _mockedLogger.ReceivedCalls().Count().Equals(2);
         
            _mockedLogger.Received(1)
                .Log(Arg.Is(LogLevel.Error),
                    Arg.Is<EventId>(0),
                    Arg.Is<object>(x =>  x.ToString() == "Exception occurred: a test internal Error"),
                    Arg.Any<Exception>(),
                    Arg.Any<Func<object, Exception?, string>>()
                );

        }

        */

