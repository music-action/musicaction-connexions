using FluentAssertions;
using MusicActionApi.Adapters.DrivenAdapters.SQLpersistence;
using MusicActionApi.Adapters.DrivenAdapters.SQLpersistence.DTO;
using MusicActionApi.HigherAbstractions.General;

namespace MusicActionApi.Tests.FakeAdapters.Tests;

public class FakePersistenceAdapterTests : IAsyncLifetime
{
    protected Lazy<ICanAccessPersistence<BoutonDTO>> _persistenceAdapterLazyForBouton;
    protected Lazy<ICanAccessPersistence<QuestionDTO>> _persistenceAdapterLazyForQuestion;
    public FakePersistenceAdapterTests() =>
        _persistenceAdapterLazyForBouton = new Lazy<ICanAccessPersistence<BoutonDTO>>(() =>
            new FakePersistenceAdapter<BoutonDTO>());
        

    [Fact()]
    public async Task TestNotNull()
    {
        _persistenceAdapterLazyForBouton.Should().NotBeNull();
    }

    public Task InitializeAsync()
    {
        _persistenceAdapterLazyForBouton = new Lazy<ICanAccessPersistence<BoutonDTO>>(() =>
            new FakePersistenceAdapter<BoutonDTO>());
        _persistenceAdapterLazyForQuestion = new Lazy<ICanAccessPersistence<QuestionDTO>>(() =>
            new FakePersistenceAdapter<QuestionDTO>());
        return Task.CompletedTask;
    }

    public Task DisposeAsync()
    {
        return Task.CompletedTask;
    }
}   