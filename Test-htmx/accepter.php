<?php
// Connexion à la base de données
$servername = "localhost";
$username = "root"; 
$password = ""; 
$dbname = "htmxtest";

// Créez la connexion
$conn = new mysqli($servername, $username, $password, $dbname);

// Vérifiez la connexion
if ($conn->connect_error) {
  die("Connexion échouée: " . $conn->connect_error);
}

// Récupérer l'ID depuis la requête GET
$id = $_GET['id'];

// Préparez et exécutez la requête SQL pour supprimer la question
$sql = "DELETE FROM QuestionsProduits WHERE Id = ?";
$stmt = $conn->prepare($sql);
$stmt->bind_param("i", $id);
$stmt->execute();

// Vérifier si la suppression a été effectuée avec succès
if($stmt->affected_rows > 0){
    echo "Success";
    echo "<script>setTimeout(function(){ window.location.reload(); }, 1000);</script>";
} else {
    echo "Error";
}

// Fermer le statement et la connexion
$stmt->close();
$conn->close();
?>
