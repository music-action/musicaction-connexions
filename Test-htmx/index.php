<?php
// Connexion à la base de données
$servername = "localhost";
$username = "root"; 
$password = ""; 
$dbname = "htmxtest";

// Créez la connexion
$conn = new mysqli($servername, $username, $password, $dbname);

// Vérifiez la connexion
if ($conn->connect_error) {
die("Connexion échouée: " . $conn->connect_error);
}

$sql = "SELECT * FROM QuestionsProduits";
$result = $conn->query($sql);
?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.tailwindcss.com"></script>
    <script src="https://unpkg.com/htmx.org@1.6.1"></script>
    <script>
        function hideRow(event) {
            // Trouver la ligne parente du bouton cliqué
            var row = event.target.closest('tr');
            // Cacher la ligne en ajoutant la classe 'hidden' (ou une autre classe de votre choix)
            row.classList.add('hidden');
        }
    </script>
</head>
<body>
<nav class="bg-white border-gray-200 dark:bg-gray-900">
    <div class="max-w-screen-xl mx-auto p-4">
        <div class="flex justify-between items-center">
            <a href="https://music-action.com/" class="flex items-center space-x-3 rtl:space-x-reverse">
                <img src="Music_Action_Logo.jpg" class="h-16" alt="Music Action Logo" />
                <span class="self-center text-2xl font-semibold whitespace-nowrap dark:text-white">Music Action</span>
            </a>
            <span class="font-semibold text-blue-600 dark:text-blue-500">Admin#12313</span>
        </div>
        <div id="mega-menu" class="mt-4 flex justify-center w-full">
            <ul class="flex flex-col md:flex-row font-medium md:space-x-8 rtl:space-x-reverse">
                <li>
                    <a href="#" class="block py-2 px-3 text-gray-900 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-600 md:p-0 dark:text-white dark:hover:bg-gray-700 dark:hover:text-blue-500">Main</a>
                </li>
                <li>
                    <div class="dropdown relative">
                        <button id="mega-menu-dropdown-button" data-dropdown-toggle="mega-menu-dropdown" class="flex items-center justify-between w-full py-2 px-3 text-gray-900 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:p-0 dark:text-white dark:hover:bg-gray-700">
                            Tableau de bord
                        </button>
                        <!-- Dropdown Menu -->
                        <div id="mega-menu-dropdown" class="absolute hidden z-10 w-44 text-base list-none bg-white rounded divide-y divide-gray-100 shadow dark:bg-gray-700">
                            <ul class="py-1" aria-labelledby="mega-menu-dropdown-button">
                                <li>
                                    <a href="#" class="block py-2 px-4 text-sm text-gray-700 hover:bg-gray-100 dark:text-gray-200 dark:hover:bg-gray-600 dark:hover:text-white">Test1</a>
                                </li>
                                <li>
                                    <a href="#" class="block py-2 px-4 text-sm text-gray-700 hover:bg-gray-100 dark:text-gray-200 dark:hover:bg-gray-600 dark:hover:text-white">Test2</a>
                                </li>
                                <li>
                                    <a href="#" class="block py-2 px-4 text-sm text-gray-700 hover:bg-gray-100 dark:text-gray-200 dark:hover:bg-gray-600 dark:hover:text-white">Test3</a>
                                </li>
                                <li>
                                    <a href="#" class="block py-2 px-4 text-sm text-gray-700 hover:bg-gray-100 dark:text-gray-200 dark:hover:bg-gray-600 dark:hover:text-white">Test4</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <a href="#" class="block py-2 px-3 text-gray-900 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-600 md:p-0 dark:text-white dark:hover:bg-gray-700 dark:hover:text-blue-500">Qui sommes-nous</a>
                </li>
                <li>
                    <a href="#" class="block py-2 px-3 text-gray-900 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:hover:text-blue-600 md:p-0 dark:text-white dark:hover:bg-gray-700 dark:hover:text-blue-500">Contact</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="relative overflow-x-auto shadow-md sm:rounded-lg">
    <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
        <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
        <tr>
            <th scope="col" class="px-6 py-3">Id Question</th>
            <th scope="col" class="px-6 py-3">Pseudo</th>
            <th scope="col" class="px-6 py-3">Email</th>
            <th scope="col" class="px-6 py-3">Crée le :</th>
            <th scope="col" class="px-6 py-3">Lien vers le produit</th>
            <th scope="col" class="px-6 py-3">Question posée</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if ($result->num_rows > 0) {
        // Sortie de chaque ligne de données
        while($row = $result->fetch_assoc()) {
        echo "<tr class='bg-white border-b dark:bg-gray-800 dark:border-gray-700'>";
            echo "<th scope='row' class='px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white'>".$row["Id"]."</th>";
            echo "<td class='px-6 py-4'>".$row["Pseudo"]."</td>";
            echo "<td class='px-6 py-4'>".$row["Email"]."</td>";
            echo "<td class='px-6 py-4'>".$row["CreeLe"]."</td>";
            echo "<td class='px-6 py-4'><a href='".$row["LienVersLeProduit"]."' class='text-blue-600 hover:underline dark:text-blue-500'>Lien vers le produit</a></td>";
            echo "<td class='px-6 py-4'>".$row["QuestionPosee"]."</td>";
            echo "</tr>";
        }
        } else {
        echo "<tr><td colspan='6' class='px-6 py-4'>Aucune question trouvée</td></tr>";
        }
        $conn->close();
        ?>
        </tbody>
    </table>
</div>

<!-- Votre contenu après la table -->

</body>
</html>
